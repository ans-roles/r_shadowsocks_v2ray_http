Role Name
=========
#### shadowsocks_v2ray_http
This role setup a shadowsocks-libev server with v2ray plugin.  
The role setup:  
   - shadowsocks-libev
   - v2ray-plugin
The role create user for starting server.
Next, the role set CAPABILITY cap_net_bind_service on v2ray-plugin. This is need for binding port number lower then 1023  
The role set ACL permission to read for server's user for certificate's files.  

the role was testing only on Debian 11

Requirements
------------

For using this role you need to have domain name and certificates. 


Role Variables
--------------
shadowsocks_addr       - listen server address (default: 0.0.0.0)
shadowsocks_port       - listen server port (default: 443) 
shadowsocks_encrypt    - encryption method (default: "xchacha20-ietf-poly1305")
shadowsocks_timeout    - timeout (default: 600 sec)
shadowsocks_nameserver - nameserver (default: 1.1.1.1)

Dependencies
------------
Nope. 

Example Playbook
----------------
       
##### playbook.yaml
    - hosts: servers
      roles:
        - shadowsocks_v2ray_http

License
-------

BSD

Author Information
------------------

khimick@gmail.com
